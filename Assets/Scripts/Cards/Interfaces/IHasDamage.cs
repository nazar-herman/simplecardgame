﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleCardGame.Cards
{
    public interface IHasDamage
    {
        float Damage { get;}
    }
}