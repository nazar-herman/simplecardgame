﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleCardGame.Cards
{
    public interface IHasHealth
    {
        float Health { get; }
    }
}