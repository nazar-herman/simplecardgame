﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleCardGame.Cards
{
    public interface ICard : IHasHealth, IHasDamage
    {
        IAction SpawnAction { get; }
        IAction OrderAction { get; }
        IAction DeathAction { get; }
    }
}