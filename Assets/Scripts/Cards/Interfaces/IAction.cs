﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleCardGame.Cards
{
    public interface IAction
    {
        void Execute(ICard card);
    }
}

